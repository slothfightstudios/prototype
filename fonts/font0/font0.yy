{
    "id": "0adb139f-2435-4d71-8576-a193f1ccfa43",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0e880672-1855-4756-8769-91183e3a5f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 121,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e7f11e69-17f9-4108-82d3-97099ce3d527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 166,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "09d806b0-9490-4202-9d4b-46528c3834f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b72b819d-3a9b-4e94-89e7-492da8978b9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9a063115-d2dd-4a17-bd79-9bb6e03e9533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "20f6a423-f901-4e13-8051-53bc85ecdfb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b74a02e5-6a86-4bae-be85-825b882e8d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "763c62c0-702f-4c7f-9b39-de776c15659b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 161,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d0db865e-21ab-466c-ac9e-0316024bdf6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8f43aa16-86c7-41f3-9840-998f5ec66c1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f6bb8dfd-e8dc-4155-ae66-0795c328ff52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "109dd7b3-dca4-4558-bd14-ebcb05618b22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ddf02a07-977a-401c-a9b1-0500010733e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 151,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "50ef15c9-17f5-4127-9b88-f0eecdc922b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "049397a6-22f2-49f6-8159-0820c51a43ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 156,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "df4ca700-3699-43e5-849e-838a284cbdef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 93,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d25778ab-4fe5-42be-873f-35da81db58e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 185,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8dcedf55-4143-43da-877f-f903654bb1bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 86,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "6826a93e-8652-4798-b24f-6f0a937d7f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 196,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2f660d62-f103-4f9b-94c5-300b595cfaf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 207,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "43b41bb6-8959-41db-bc63-443d2389cee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bef6d4ce-7508-4d1a-a4df-08121ca10dc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8449af7c-87bd-4acb-8c76-aeafe6885e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "df659f25-46e2-42d7-b94c-70d27c4f33ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "018e5cd5-6ed3-4eb4-8678-d538c9f8e05a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8eca46bf-af9e-4bc0-83ef-9ae575253d93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c3ce7bca-d8af-4230-86a5-feca2ffd8fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d40fd431-43fa-4a6d-bf75-b340b8b2349f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0dcbd125-48ca-47b8-b922-94145e0a7c9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 42
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d5a28d11-5545-4c6f-98aa-62c8c3bcd462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 42
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2a6260a6-9ec8-43ce-97a0-82566fa0d7d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 174,
                "y": 22
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2e28ccad-8390-4a6d-925b-f8d007e6dcab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 22
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6e7362ad-ea7a-44a9-ba4d-83f82f3516ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c58b7bc9-dd2e-4b70-a2c4-398622fa1c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "962ec384-570f-41f2-afa8-641717b71afb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 141,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ae3b6cde-7672-421e-83fa-b762677db626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1858fb7e-4993-4f1c-9706-38e11422eac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d1eb616b-0deb-4634-8518-334119a29cf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 130,
                "y": 22
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e40a941b-0a8f-4472-bb4c-01cfe89e6687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 119,
                "y": 22
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e4fa9f4d-f647-4581-bc25-5fe48ca73470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d9033bbd-19ed-4f5b-9dc6-3b73f1aa7861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "33900da2-f855-4f76-98a6-32085d5a2f5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 171,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "618e4447-5e6a-46dc-84a2-20614793f171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "63db1489-450a-4d37-bb48-0726229f0f7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 22
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f8ccae79-0b26-4546-9641-7291ed7e1d80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f9048aab-75e9-4934-81c4-89d21016aaa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7504efbc-7964-4772-b91d-f318a4a60368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7ee5e8af-0ac7-48e8-a704-643d4b19243e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "09fa7f5e-4b56-43d8-98b3-033d495260e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 152,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ffc1452c-1f87-4999-96ba-7de0affab5a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9ca7aaf3-59dc-4c34-8a90-9e30e9993e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2fc8d751-6658-47df-ac71-2a1e57970da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 22
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "94631c41-ae23-46b4-93e7-496642ef921f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "129efcf3-ff1a-48eb-bbbb-432d407d4023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 22
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5a622fc3-b6e1-42a1-9422-04d312fdd1ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "eee27c93-2e39-4d64-ac67-387acd3c006c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0e00abe4-222c-44ea-97d2-3863026bf18a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "21428c81-ce8f-437d-bb16-bc1e4a5fb420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7f036d2a-879a-4c9e-bc04-19b28d8a46f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 22
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "34b93008-092e-4167-909e-a89e076cc22f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 145,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8a17296e-2631-4d63-bb8e-c3acafa0884d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "12920c2b-0006-4f96-9a8a-c64ca2da0a21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 127,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "59acc1b5-b1a1-42e2-a921-95f3c16c7eb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 131,
                "y": 42
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "63eac231-098e-4b98-b6d4-07a1b7868ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "354bc327-7ba9-4254-bc1f-60a25304da8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 133,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a2f2ce0d-ef28-4be5-84d9-8c504dd9b439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 97,
                "y": 22
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "38069aa1-323c-4956-8c9b-48af47622717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7a9766fc-0cc0-4dc0-98bb-1ae3a212d29b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 151,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a75de836-28ec-47c0-9d40-b9cf4fd7a965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 42
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4454051d-3b8d-4720-9ace-527243807505",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 108,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "77ea2129-60fd-41ca-b850-f5fc9e5eea32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "97d3cbc9-de94-4433-af71-381fc5865cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 161,
                "y": 42
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7c45ede0-5394-45fa-8587-465a13307b2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 231,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4afce210-f5df-42d1-9bcb-c5d80fa3da09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 194,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f0c9ae59-5fa2-4848-a0be-0475c4e347af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 139,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "bed3d3c9-5080-4535-a1f8-0ab3959d85ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "43563b2d-d736-44cb-8892-97d8ce4cc850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3d3b1ed5-682e-4726-b852-25917715eae9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a5adfbb1-aba6-41f9-b352-7e4ec433418f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 240,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3defa3f3-82d1-4da4-a49d-64cb18f20117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e22cdfdc-fa28-44f3-b51b-db23aac13b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 141,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "435519d1-63c6-48b8-a7ce-7d84069eb235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 111,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "57781edb-cfd5-4211-8e08-cd4bc0701773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ff6fe629-6136-4a67-9bb5-d8086a4bda1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ed0f4d9c-3f6a-4b6d-9dda-6cc689800375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8824cd3f-1ffa-49bb-a060-0f20da6d3f53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1ca3a9d9-3c9a-41aa-97c7-57352041ee0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 181,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "54d1c85f-41ee-4280-92e6-1614ff877a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "624ee685-1feb-4830-8036-6226e5ea5307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5b9c9c2d-af26-4ba2-97f7-dc2bbefe84d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "72a42e75-117a-4357-af82-faa0c8759122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 42
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ca5831b9-749b-4081-aecf-207d225e6340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "dc5160cc-ce96-4769-9446-b713d73753ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 190,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a4a232f5-af7c-4f84-88bf-5ad73b5e40ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "708bc53d-1416-4c94-8675-5ad2c8f8a905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 163,
                "y": 22
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "340f8c49-4dd9-4d66-8a9e-cc5deba7ff20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "10255752-a263-482b-933a-08374584aee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "e926faa3-4dfa-4216-80b3-4afc43b37a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "f6b355a0-daae-4dd5-8e79-fd3e4ed767d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "646af270-64de-4a5d-9ab1-fbda0ef6aa42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "89e9ca4f-b431-4d2a-9b89-5152af7924f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "75b99723-2312-4f36-9964-b35107074644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "ac461b78-fce0-4f08-b1a3-6da5438d53fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "913daf6d-6df4-4615-b24f-c871359ac308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "606e1870-a37a-40b7-8de8-f900363ec593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "044703cc-b633-463b-8f57-5dfde05bb636",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "fafe5f9f-0e37-496d-8d97-d4297b89b7b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "740bc4ae-e59b-480d-98e2-86482222e72c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "edbb1ce8-cb48-4c85-8656-809ff6aa5ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "142cfe6e-23a5-4371-812f-be830c9482eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "1659e466-89a4-487d-8b36-a334c7c5923b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "a02372a0-b154-4d94-b650-51c74a019522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "256e5904-2fd3-4317-9428-8071aeae89a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "49e2614b-b4be-4f72-816b-ac8581af51c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "554b08df-8516-48d0-aa97-9775b9c71b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "7a5d13a3-850d-4da7-9c32-06a0ad73fca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "5f8a05af-764b-48fd-b4b2-94943cf2bac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "09ca3728-7d5f-4f06-b5a6-7111fc6b17c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "4b3d6090-4d73-4bc0-afd5-00de06fecbcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "edac8163-b231-442c-9bed-ecd6119567f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "7fd99e07-e3e7-4469-8050-24912a049d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "52d05e76-ef2f-4cf6-9de5-831533e68aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "8eb787e4-889f-4946-808e-b2e91085083f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "41ce954b-01df-4467-84d2-df86a4753636",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "869d510d-ac15-4806-8d61-90b10e107e8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "f55ef97b-d2db-4563-8319-79cd2f0952cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "c82eb359-9624-424f-8d70-df98cc65d00c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "ee700089-c726-4edb-98d8-7cd2e1cf7e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "0908606a-8b80-428e-a8b3-eec80ac017f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "b6c539b8-8b6a-4ab9-94c7-f2415bc8b4eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "088c88c5-21ba-4df1-bb9f-802dcb918b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "4784e140-5cc1-43bb-8383-ac4ff233b2e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "1d7fb500-8e97-46ec-a79b-037ffae44b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "4b5bdeda-a481-4c05-8810-2b5331f9ce30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "457d0392-efdb-491c-bcbe-017a2ad194b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "8346e8d4-b5b9-45cc-aaa8-fc7b3233ac68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "8f5367d7-7d8d-4554-a280-d9103172ea54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "f410d6e3-b316-4ca5-b0b1-48e5a565b70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "62885572-edd3-4edc-8ddd-e3f79021c5bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "1916db7a-78de-4444-bdb9-df400a67bcbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "d79f8b74-96f1-4b7b-8c77-ede3aeb128a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "0b484dd2-2ca4-45b3-906b-89ce7ddc6be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "79b6e112-fdaf-4472-9399-1341bb5d489b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "60c58b02-188f-4046-bf57-36de02f1f707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "2d280356-7961-4fc2-9c06-ab2839fbe74f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "507c3010-b929-4ac5-b8de-393db931b187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "4aed19a7-969b-4181-b4a9-d6dbcb34e429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "2da62041-de59-46eb-846d-22883b9d5912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "9503d44e-b57a-4fcf-83a9-24e4158eb715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "f27cf412-2981-4bed-81be-1ef29e72654d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "ef5cfa34-0489-4172-b6ea-6ea2d3bf3103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "df673213-bd2e-4116-8787-7bfe85eb3d98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "e1e2ee62-ec59-40e0-8ba4-34cdee567259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "d05189c4-09ba-4b5e-8570-e1c2ecf0963c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "544dc9f3-642c-4cc6-a6f4-f0e58f0aa30b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "25a95a89-e6d7-4efc-8685-ebfae1d53d64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "14619069-c282-4c19-a9b7-7756c57d3a18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "f0c071ee-6b46-45c3-a847-9be6c960f7e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "517005a4-3a3f-45c9-b7d2-81654e6ffd17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "fd0c3e8d-433d-4309-936f-c4ceeeefcfff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "8e27569e-d948-4eca-a46a-41d0209a05a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "b6d573a7-37ac-4db9-a7b9-3389aadca78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "826bf338-d293-4d8f-9928-796167b81a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "e4be953c-962a-4c53-9c95-5434c1c1b54d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "1190e94c-bb37-477a-a0a0-adf5e773b8bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "2b10e262-b8b7-48cc-98b2-67e405755727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}