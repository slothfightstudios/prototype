{
    "id": "21267bdb-e23b-4039-9c97-f04f0b53773f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2a3e2a97-9b8f-4225-b50d-14364a3a2728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21267bdb-e23b-4039-9c97-f04f0b53773f",
            "compositeImage": {
                "id": "3d617649-956c-49b2-a1b2-84ba1371e19d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a3e2a97-9b8f-4225-b50d-14364a3a2728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71e9032e-06d7-4bfe-af60-587cdf0d2995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a3e2a97-9b8f-4225-b50d-14364a3a2728",
                    "LayerId": "68739872-abe9-43f9-bdab-398f1d49c485"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "68739872-abe9-43f9-bdab-398f1d49c485",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21267bdb-e23b-4039-9c97-f04f0b53773f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 16,
    "yorig": 16
}