{
    "id": "d7bd85b8-04b3-41d6-b5e6-e0b2cdd480df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite012",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e26a6a5e-b2be-482f-915d-0295571620f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7bd85b8-04b3-41d6-b5e6-e0b2cdd480df",
            "compositeImage": {
                "id": "5f16fe5b-0bcb-4a60-893a-1ba14c6b5265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e26a6a5e-b2be-482f-915d-0295571620f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "448b7058-1dda-4591-9361-cdab1985b461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e26a6a5e-b2be-482f-915d-0295571620f2",
                    "LayerId": "00ff4ec4-b975-4e79-a277-d5f9be8dbf40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "00ff4ec4-b975-4e79-a277-d5f9be8dbf40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7bd85b8-04b3-41d6-b5e6-e0b2cdd480df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}