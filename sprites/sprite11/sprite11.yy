{
    "id": "1079f55a-9090-48f3-b8cb-167977078033",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "39baec7f-44a3-4bea-b62d-5528c9e5ae97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1079f55a-9090-48f3-b8cb-167977078033",
            "compositeImage": {
                "id": "e70a8256-76ad-4f53-8d81-32957f9cac11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39baec7f-44a3-4bea-b62d-5528c9e5ae97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e84f1567-00c4-4909-8db3-4d8d5eea7d41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39baec7f-44a3-4bea-b62d-5528c9e5ae97",
                    "LayerId": "d3327644-43c5-4810-9998-24fea3ee1c71"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "d3327644-43c5-4810-9998-24fea3ee1c71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1079f55a-9090-48f3-b8cb-167977078033",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}