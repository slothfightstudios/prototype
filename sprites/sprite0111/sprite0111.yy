{
    "id": "2a87229e-8663-4f10-b96a-ed58eea72200",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0111",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45d426ab-c789-4109-b931-8037d592a012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a87229e-8663-4f10-b96a-ed58eea72200",
            "compositeImage": {
                "id": "1f8bcb0c-d418-4032-aec1-47ac32a29d83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d426ab-c789-4109-b931-8037d592a012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b67ae521-04e6-4208-bc3d-34701b62007f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d426ab-c789-4109-b931-8037d592a012",
                    "LayerId": "79863fb6-88a6-4eaf-85c4-a1c8c68fc148"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "79863fb6-88a6-4eaf-85c4-a1c8c68fc148",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a87229e-8663-4f10-b96a-ed58eea72200",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}