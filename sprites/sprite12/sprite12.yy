{
    "id": "6db9446a-99e0-4491-b853-070c51594b66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57579aa0-f067-42a6-aad6-cb5fe4d90d51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db9446a-99e0-4491-b853-070c51594b66",
            "compositeImage": {
                "id": "73c944e5-b1e0-4583-9a14-b307687074b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57579aa0-f067-42a6-aad6-cb5fe4d90d51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be45c5a0-b457-42b2-b381-e58c3759f540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57579aa0-f067-42a6-aad6-cb5fe4d90d51",
                    "LayerId": "4813895b-b76f-46f7-b9a9-22cc6614b18d"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "4813895b-b76f-46f7-b9a9-22cc6614b18d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6db9446a-99e0-4491-b853-070c51594b66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}