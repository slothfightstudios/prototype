{
    "id": "af59159a-a528-4a1a-96d3-6625a95ad61e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1554c0c5-8aa7-430a-926f-95bea30f99e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af59159a-a528-4a1a-96d3-6625a95ad61e",
            "compositeImage": {
                "id": "1f4a51cb-9b5c-4a75-937e-c274025e300f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1554c0c5-8aa7-430a-926f-95bea30f99e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54ca7d9e-2676-4d77-bb9a-4106fc429fee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1554c0c5-8aa7-430a-926f-95bea30f99e0",
                    "LayerId": "8df651a9-8b7a-48d3-bc8f-b4850207855d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8df651a9-8b7a-48d3-bc8f-b4850207855d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af59159a-a528-4a1a-96d3-6625a95ad61e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}