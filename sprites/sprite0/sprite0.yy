{
    "id": "871da166-cb9d-4ef8-b87b-c6658903465b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 2,
    "coltolerance": 0,
    "frames": [
        {
            "id": "438f8ff8-2cbe-4b19-ab75-594dd36a8122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "871da166-cb9d-4ef8-b87b-c6658903465b",
            "compositeImage": {
                "id": "34d82812-f546-49a7-baec-fdc0ddcdb971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "438f8ff8-2cbe-4b19-ab75-594dd36a8122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8537ab0f-7fa1-4c45-a0d6-2548e01756d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "438f8ff8-2cbe-4b19-ab75-594dd36a8122",
                    "LayerId": "8fc1c648-ba80-4406-8727-e369746c4a3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8fc1c648-ba80-4406-8727-e369746c4a3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "871da166-cb9d-4ef8-b87b-c6658903465b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}