{
    "id": "519a2b0d-5ea4-431a-a1cf-680884ccd24e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fd62adc8-defc-4c63-8da5-9acb3035d9d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "519a2b0d-5ea4-431a-a1cf-680884ccd24e",
            "compositeImage": {
                "id": "25993f28-c7ca-4027-a526-7c913f58c52d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd62adc8-defc-4c63-8da5-9acb3035d9d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd2c5f8-8f9d-4c0b-841f-e23efc253f3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd62adc8-defc-4c63-8da5-9acb3035d9d5",
                    "LayerId": "4a808e27-56a3-43cc-b4a9-1605c81ca6bf"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "4a808e27-56a3-43cc-b4a9-1605c81ca6bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "519a2b0d-5ea4-431a-a1cf-680884ccd24e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}