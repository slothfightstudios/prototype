{
    "id": "68c75c9c-d683-4915-b1b4-3eb000b86189",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite011",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 2,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f60ab700-717e-4d04-a51c-13c598a389fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68c75c9c-d683-4915-b1b4-3eb000b86189",
            "compositeImage": {
                "id": "141ef140-e0e5-46ed-a6f7-ac575360d7c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f60ab700-717e-4d04-a51c-13c598a389fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b2d924e-8b1b-4456-a106-77ffca802f37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f60ab700-717e-4d04-a51c-13c598a389fd",
                    "LayerId": "55c248ca-73e2-4e07-9f14-8863d43135b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "55c248ca-73e2-4e07-9f14-8863d43135b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68c75c9c-d683-4915-b1b4-3eb000b86189",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}