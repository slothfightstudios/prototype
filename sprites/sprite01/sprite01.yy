{
    "id": "98ba01aa-9fe3-47c7-b3a0-09c5068f180d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 2,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3bb11985-fa25-4514-a86f-040e5d2c2d71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98ba01aa-9fe3-47c7-b3a0-09c5068f180d",
            "compositeImage": {
                "id": "ec319da3-1eda-4e12-b447-61c7e7205130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bb11985-fa25-4514-a86f-040e5d2c2d71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40c9c6af-8e1d-4c10-887a-85bfca6c8626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bb11985-fa25-4514-a86f-040e5d2c2d71",
                    "LayerId": "da7058f2-8f97-4fbb-9555-e19f6f17e8a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "da7058f2-8f97-4fbb-9555-e19f6f17e8a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98ba01aa-9fe3-47c7-b3a0-09c5068f180d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}