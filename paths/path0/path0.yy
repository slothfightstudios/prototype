{
    "id": "df7accbc-deb4-45cb-90a7-70a9266ff448",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path0",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "eba826b3-2d74-4c87-abb9-bdac45e43ca3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 368,
            "y": 16,
            "speed": 100
        },
        {
            "id": "15e3089c-3aa6-46d2-aa16-b914c6d97e5c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 368,
            "y": 80,
            "speed": 100
        },
        {
            "id": "b33cc05f-3905-4baa-bd2a-b9b33c11ef05",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 368,
            "y": 128,
            "speed": 100
        },
        {
            "id": "feeec3f8-5bed-48b4-93f3-37ae4a015137",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 688,
            "y": 128,
            "speed": 100
        },
        {
            "id": "fff7f729-f28e-4619-912b-0acb2f9dffed",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 688,
            "y": 224,
            "speed": 100
        },
        {
            "id": "1e866020-3d03-43da-9c9b-a1f0a06c68a0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 400,
            "y": 224,
            "speed": 100
        },
        {
            "id": "91b5d16f-0f46-43fa-bed9-1dfa57bca47e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 400,
            "y": 320,
            "speed": 100
        },
        {
            "id": "73b7bfe3-c674-4c38-a175-eb0ac755b290",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 656,
            "y": 320,
            "speed": 100
        },
        {
            "id": "9afaca63-77cc-4edf-8393-a0c6b54033aa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 656,
            "y": 416,
            "speed": 100
        },
        {
            "id": "9e65d57a-de70-4f76-a9bd-6ee433a2c090",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 432,
            "y": 416,
            "speed": 100
        },
        {
            "id": "cf164add-1e9c-43a3-bdcf-a2bb7e535153",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 432,
            "y": 512,
            "speed": 100
        },
        {
            "id": "f1bf1b82-d628-4749-96d9-657890b169ce",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 624,
            "y": 512,
            "speed": 100
        },
        {
            "id": "46710485-32c1-4d84-9f69-578aa5fc0a14",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 624,
            "y": 608,
            "speed": 100
        },
        {
            "id": "e972095a-42f7-4381-86c0-dedaaafffc80",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 528,
            "y": 608,
            "speed": 100
        },
        {
            "id": "a5e36ebb-b24f-405a-9071-3f4e85c05df1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 528,
            "y": 768,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}