{
    "id": "9634ac96-678d-4442-b321-c90e99321d8f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_plr",
    "eventList": [
        {
            "id": "80eecd9a-d8fe-4cd1-8d5e-d70ba413078e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9634ac96-678d-4442-b321-c90e99321d8f"
        },
        {
            "id": "8186f1df-f52d-4f1a-b77e-0312e1c24285",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9634ac96-678d-4442-b321-c90e99321d8f"
        },
        {
            "id": "05660b5a-b1cc-4b6b-b02c-cbcff8bce958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "48f0e559-20ac-4709-883d-87dc94735689",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9634ac96-678d-4442-b321-c90e99321d8f"
        },
        {
            "id": "0731651c-ca8c-450a-a5b3-064c24b19a23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9634ac96-678d-4442-b321-c90e99321d8f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "98ba01aa-9fe3-47c7-b3a0-09c5068f180d",
    "visible": true
}