{
    "id": "7bf0738a-f230-48b5-a20c-5f584a242b73",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ui_tower_buildpoint",
    "eventList": [
        {
            "id": "136f8d4a-784b-4639-8ee9-3d3c287bfc35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "7bf0738a-f230-48b5-a20c-5f584a242b73"
        },
        {
            "id": "e26de405-1b7d-4d27-9f94-c4742965deb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7bf0738a-f230-48b5-a20c-5f584a242b73"
        },
        {
            "id": "2ae0f15c-4c4d-432d-8446-f97847cb3453",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7bf0738a-f230-48b5-a20c-5f584a242b73"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68c75c9c-d683-4915-b1b4-3eb000b86189",
    "visible": true
}