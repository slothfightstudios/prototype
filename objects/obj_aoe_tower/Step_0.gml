/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4E9AC82D
/// @DnDArgument : "code" "var xx //Store tower position for creep use$(13_10)var yy$(13_10)xx = x$(13_10)yy = y$(13_10)$(13_10)if (can_shoot == 1) //Check if Tower is able to shoot$(13_10){$(13_10)	with obj_creep //This makes creeps check if they're in range of tower aoe & take damage if true$(13_10)	{$(13_10)		dis = point_distance(x, y, xx, yy);$(13_10)		if (dis < 64) hp -= 25;$(13_10)	}$(13_10)	fire = 1; //Indicate the Tower is firing$(13_10)	can_shoot = 0; //Indicate Tower is on cooldown$(13_10)	alarm_set(0, 90); //Repeat Alarm 0 in 45 steps$(13_10)	alarm_set(1, 15); //Alarm 1 set to go off in 5 steps (duration of firing effect)$(13_10)}"
var xx //Store tower position for creep use
var yy
xx = x
yy = y

if (can_shoot == 1) //Check if Tower is able to shoot
{
	with obj_creep //This makes creeps check if they're in range of tower aoe & take damage if true
	{
		dis = point_distance(x, y, xx, yy);
		if (dis < 64) hp -= 25;
	}
	fire = 1; //Indicate the Tower is firing
	can_shoot = 0; //Indicate Tower is on cooldown
	alarm_set(0, 90); //Repeat Alarm 0 in 45 steps
	alarm_set(1, 15); //Alarm 1 set to go off in 5 steps (duration of firing effect)
}