{
    "id": "2f88c63a-712f-4ea5-a9be-7018439a9597",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_aoe_tower",
    "eventList": [
        {
            "id": "708bb6d7-1d68-44d8-ad94-33233cd4c616",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f88c63a-712f-4ea5-a9be-7018439a9597"
        },
        {
            "id": "f0ffe3a3-2d3b-4424-8d11-80d8b3f4cae9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2f88c63a-712f-4ea5-a9be-7018439a9597"
        },
        {
            "id": "253b1dbc-96fc-425b-98cd-c00925eacaa5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2f88c63a-712f-4ea5-a9be-7018439a9597"
        },
        {
            "id": "b73e620c-5993-43eb-8ab0-5da6bc461098",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2f88c63a-712f-4ea5-a9be-7018439a9597"
        },
        {
            "id": "bc90520f-ff72-455a-ab16-c9f5b754333c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2f88c63a-712f-4ea5-a9be-7018439a9597"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a87229e-8663-4f10-b96a-ed58eea72200",
    "visible": true
}