{
    "id": "b6f5a0b8-50c3-4e2b-b6c4-2f45eb4813c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bubble",
    "eventList": [
        {
            "id": "23c56c40-6259-4fce-945f-4249514f3f7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9634ac96-678d-4442-b321-c90e99321d8f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b6f5a0b8-50c3-4e2b-b6c4-2f45eb4813c4"
        },
        {
            "id": "e3b45bc6-4882-46a1-81bc-97589ba1656c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b6f5a0b8-50c3-4e2b-b6c4-2f45eb4813c4"
        },
        {
            "id": "bbe604a1-3a55-45ee-8785-736d569192e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b6f5a0b8-50c3-4e2b-b6c4-2f45eb4813c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "21267bdb-e23b-4039-9c97-f04f0b53773f",
    "visible": true
}