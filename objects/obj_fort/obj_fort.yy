{
    "id": "954afe68-e51e-4490-90ff-293a740d356c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fort",
    "eventList": [
        {
            "id": "4d5067b1-50cf-4d7b-b5e3-34b728f11384",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "954afe68-e51e-4490-90ff-293a740d356c"
        },
        {
            "id": "375c9334-31c8-4c11-ba8b-342e708c17c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "78708760-322f-412a-bebd-b9bc9737acb3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "954afe68-e51e-4490-90ff-293a740d356c"
        },
        {
            "id": "96ab9387-3c62-4996-820d-03f89502464f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "954afe68-e51e-4490-90ff-293a740d356c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1079f55a-9090-48f3-b8cb-167977078033",
    "visible": true
}