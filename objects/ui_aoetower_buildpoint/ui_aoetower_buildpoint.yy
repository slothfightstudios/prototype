{
    "id": "0a5b52c3-4051-42dd-838e-e4df97082ee4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ui_aoetower_buildpoint",
    "eventList": [
        {
            "id": "afafdca4-d94e-413f-ab23-f981bb84697c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "0a5b52c3-4051-42dd-838e-e4df97082ee4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7bf0738a-f230-48b5-a20c-5f584a242b73",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a87229e-8663-4f10-b96a-ed58eea72200",
    "visible": true
}