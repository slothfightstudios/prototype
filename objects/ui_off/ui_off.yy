{
    "id": "b32dddb8-8d4f-436a-9e17-da4cd58cb39f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ui_off",
    "eventList": [
        {
            "id": "df33ef01-cca7-46ba-8bb8-280fc8d9c43a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "b32dddb8-8d4f-436a-9e17-da4cd58cb39f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ae5157e8-de4a-4c27-ba25-07b8924ea15d",
    "visible": true
}