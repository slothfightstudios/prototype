/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 17040308
/// @DnDArgument : "code" "var num_creeps = instance_number(creeps[0,1]); //Count the total number of creeps and set variable "creeps" to that amount$(13_10)$(13_10)i = irandom(1); //Pick a random number equivacal to creep varieties (1 less than total number)$(13_10)if (num_creeps <= 4) instance_create_depth(370, 15, 99, choose(creeps[i])); //If there are less than 5 creeps, spawn another one at random at the beginning of the path$(13_10)alarm_set(0, 30); //Repeat alarm in 30 sec"
var num_creeps = instance_number(creeps[0,1]); //Count the total number of creeps and set variable "creeps" to that amount

i = irandom(1); //Pick a random number equivacal to creep varieties (1 less than total number)
if (num_creeps <= 4) instance_create_depth(370, 15, 99, choose(creeps[i])); //If there are less than 5 creeps, spawn another one at random at the beginning of the path
alarm_set(0, 30); //Repeat alarm in 30 sec