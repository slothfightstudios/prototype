{
    "id": "d7c8eddc-3902-485c-8e02-6c99014a120c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_creep2",
    "eventList": [
        {
            "id": "d5ef1c91-8a6e-4dd3-86b0-ec2ca63e2a9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7c8eddc-3902-485c-8e02-6c99014a120c"
        },
        {
            "id": "e9d95519-75df-4b67-b5e3-a8461d2b5c1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d7c8eddc-3902-485c-8e02-6c99014a120c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "78708760-322f-412a-bebd-b9bc9737acb3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "af59159a-a528-4a1a-96d3-6625a95ad61e",
    "visible": true
}