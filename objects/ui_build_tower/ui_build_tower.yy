{
    "id": "94ae5204-600b-454d-bfc5-c0ad9c476567",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ui_build_tower",
    "eventList": [
        {
            "id": "c97642b4-5455-444f-bb37-b3924ff71f56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "94ae5204-600b-454d-bfc5-c0ad9c476567"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68c75c9c-d683-4915-b1b4-3eb000b86189",
    "visible": true
}