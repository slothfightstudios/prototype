{
    "id": "78708760-322f-412a-bebd-b9bc9737acb3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_creep",
    "eventList": [
        {
            "id": "1a14264a-6028-4279-96fb-06dc7721b8ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78708760-322f-412a-bebd-b9bc9737acb3"
        },
        {
            "id": "7bddee7e-cd7e-4452-89e9-51f5cfaa8fa0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78708760-322f-412a-bebd-b9bc9737acb3"
        },
        {
            "id": "e280b346-dd1a-45ae-ab87-287547b3e993",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "78708760-322f-412a-bebd-b9bc9737acb3"
        },
        {
            "id": "b7c4a4f6-73fd-428d-a2cf-396f6f17e008",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "78708760-322f-412a-bebd-b9bc9737acb3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "871da166-cb9d-4ef8-b87b-c6658903465b",
    "visible": true
}