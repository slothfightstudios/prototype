{
    "id": "c10adbf5-ba29-4244-abbb-0b9bdb1e9e1c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tower",
    "eventList": [
        {
            "id": "670736e0-84ac-4e97-9eb7-abd7cfd1a9f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c10adbf5-ba29-4244-abbb-0b9bdb1e9e1c"
        },
        {
            "id": "a408f63d-9651-43ac-ab0f-0b775505b0ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c10adbf5-ba29-4244-abbb-0b9bdb1e9e1c"
        },
        {
            "id": "b082442a-c2bb-49f6-9125-3795f6a4188b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c10adbf5-ba29-4244-abbb-0b9bdb1e9e1c"
        },
        {
            "id": "e6c332ef-e065-488a-a96c-6cdc7df79779",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c10adbf5-ba29-4244-abbb-0b9bdb1e9e1c"
        },
        {
            "id": "bf416cba-ffa7-4dfd-8c1c-7b32ced59d9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c10adbf5-ba29-4244-abbb-0b9bdb1e9e1c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68c75c9c-d683-4915-b1b4-3eb000b86189",
    "visible": true
}