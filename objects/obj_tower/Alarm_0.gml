/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 53FFCF8B
/// @DnDArgument : "code" "///Tower shoot cycle reset$(13_10)$(13_10)can_shoot = 1; //Tower is able to shoot$(13_10)fire = 0; //Tower is no longer shooting (redundancy)"
///Tower shoot cycle reset

can_shoot = 1; //Tower is able to shoot
fire = 0; //Tower is no longer shooting (redundancy)