/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4E9AC82D
/// @DnDArgument : "code" "if (can_shoot == 1) //Check if Tower is able to shoot$(13_10){$(13_10)	inst = instance_nearest(x, y, obj_creep); //find nearest creep; set it's ID as variable "inst"$(13_10)	$(13_10)	if (distance_to_object(inst) < 50) //Check if target is in range, execute shoot actions if so$(13_10)	{$(13_10)		fire = 1; //Indicate the Tower is firing$(13_10)		xx = inst.x; //Creep position for effect use$(13_10)		yy = inst.y;$(13_10)		inst.hp -= 50; //reduce targets hitpoints by 50$(13_10)		can_shoot = 0; //Indicate Tower is on cooldown$(13_10)		alarm_set(0, 45); //Repeat Alarm 0 in 45 steps$(13_10)		alarm_set(1, 5); //Alarm 1 set to go off in 5 steps (duration of firing effect)$(13_10)	}$(13_10)}$(13_10)$(13_10)if instance_exists(inst) image_angle = point_direction(x, y, inst.x, inst.y) - 90; //Tower look at target"
if (can_shoot == 1) //Check if Tower is able to shoot
{
	inst = instance_nearest(x, y, obj_creep); //find nearest creep; set it's ID as variable "inst"
	
	if (distance_to_object(inst) < 50) //Check if target is in range, execute shoot actions if so
	{
		fire = 1; //Indicate the Tower is firing
		xx = inst.x; //Creep position for effect use
		yy = inst.y;
		inst.hp -= 50; //reduce targets hitpoints by 50
		can_shoot = 0; //Indicate Tower is on cooldown
		alarm_set(0, 45); //Repeat Alarm 0 in 45 steps
		alarm_set(1, 5); //Alarm 1 set to go off in 5 steps (duration of firing effect)
	}
}

if instance_exists(inst) image_angle = point_direction(x, y, inst.x, inst.y) - 90; //Tower look at target