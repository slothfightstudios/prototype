{
    "id": "4e2ab19f-6dc2-4577-b22b-fe6de54ab5f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ctrl_bubbles",
    "eventList": [
        {
            "id": "cbbb4026-9c1f-44eb-8cf5-d0ae8ef8129b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e2ab19f-6dc2-4577-b22b-fe6de54ab5f1"
        },
        {
            "id": "59e29870-87a4-49f9-8d03-728946c85760",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4e2ab19f-6dc2-4577-b22b-fe6de54ab5f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}