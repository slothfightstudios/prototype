var bubbles = instance_number(obj_bubble); //Count the total number of bubbles and set variable "bubbles" to that amount
var rand = choose(750, 150, 300, 450, 600);
if (bubbles <= 4) instance_create_depth(rand, 768, 98, obj_bubble); //If there are less than 5 bubbles, spawn another one at the beginning of the path
alarm_set(0, 30); //Repeat alarm in 30 sec