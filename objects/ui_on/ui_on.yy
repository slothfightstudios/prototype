{
    "id": "51086bb2-c072-449d-91d0-a2d4cc112f9e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ui_on",
    "eventList": [
        {
            "id": "53b9da24-c213-4324-9bd0-61dc02c7e1f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "51086bb2-c072-449d-91d0-a2d4cc112f9e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7bd85b8-04b3-41d6-b5e6-e0b2cdd480df",
    "visible": true
}