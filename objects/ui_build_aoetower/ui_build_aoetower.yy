{
    "id": "f2c3a611-855a-40a2-84ae-f32baf6434fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ui_build_aoetower",
    "eventList": [
        {
            "id": "8ec8abd7-0f6d-4145-ad2c-1dcb2175a8d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "f2c3a611-855a-40a2-84ae-f32baf6434fb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a87229e-8663-4f10-b96a-ed58eea72200",
    "visible": true
}